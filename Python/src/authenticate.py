import os, json
import typer
from datetime import datetime, timedelta
from requests import post
from requests.status_codes import codes


CACHE_PATH = '.auth0_cache'

def authenticate(
    context: typer.Context,
    domain = typer.Option(..., envvar='AUTH0_DOMAIN'),
    client_id = typer.Option(..., envvar='AUTH0_CLIENT_ID'),
    client_secret = typer.Option(..., envvar='AUTH0_CLIENT_SECRET'),
    audience = typer.Option(..., envvar='AUTH0_AUDIENCE')
):
    """
    Authenticates via the supplied Auth0 domain, using the supplied
    credentials, and adds the access_token to the Typer app context. If 
    the method parameters are omitted, the named environment variables
    are used instead. This method is expected to be used as a callback
    in a Typer app. It will be invoked before the specified command is 
    called. Credentials will be cached locally, based on their expiry,
    to avoid excessive authentication requests to Auth0.
    """
    context.obj = {'access_token': None}
    cache:dict = None
    now = datetime.utcnow()

    if os.path.exists(CACHE_PATH):
        with open(CACHE_PATH, 'r') as fp:
            cache = json.load(fp)

    if not cache or datetime.fromisoformat(cache['expires_at']) < now:
        typer.echo(f'Authenticating via {domain}')

        headers = {
            'Content-Type': 'application/json'
        }

        payload = json.dumps({
            'grant_type': 'client_credentials',
            'client_id': client_id,
            'client_secret': client_secret,
            'audience': audience
        })

        response = post(
            f'{domain}/oauth/token',
            data=payload,
            headers=headers)

        # If we don't recieve a success status, abort
        response.raise_for_status()

        cache = response.json()
        cache['expires_at'] = (
            datetime.utcnow() + timedelta(seconds=cache['expires_in'])
        ).isoformat()
    else:
        typer.echo('Using cached access token')

    with open(CACHE_PATH, 'w') as fp:
        fp.write(json.dumps(cache))

    context.obj['access_token'] = cache['access_token']
