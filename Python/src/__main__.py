"""Python RightShip API integration samples
"""

import os
import typer
from dotenv import load_dotenv

from .authenticate import authenticate
from .vessels import vessels


def run():
    """Runs the relevant sample based on the command line arguments"""
    
    # Dotenv is used for testing purposes, it allows local `.env` files to
    # override the system environment variables. It is not required and you would 
    # not normally use a `.env` file in a deployed scenario.
    if os.path.exists('.env'):
        load_dotenv('.env')

    # Typer (https://typer.tiangolo.com/) is used for the command line interface
    # Typer helps to keep the code simpler and offers other benefits, but is not
    # required, manual parsing using `sys.argv` is also possible
    app = typer.Typer(callback=authenticate)
    app.add_typer(vessels)

    # Run the configured CLI app, which will process the command line arguments 
    # and trigger the appropriate sample, along with any supplied arguments.
    app()


if __name__ == '__main__':
    run()
