import json
import csv
import typer
from requests import post


vessels = typer.Typer(name='vessels')

@vessels.command()
def download_ghg_ratings(
    context: typer.Context,
    api_vessels_endpoint = typer.Argument(..., envvar='API_VESSELS_ENDPOINT'),
    ghg_ratings_csv_path = typer.Option('ghg_ratings.csv')
):
    """
    Queries the RightShip Vessels API with a list of IMOs. If a
    successful response is returned, the GHG ratings (and related data)
    are extracted from the response and saved to a local CSV file. If 
    the method parameters are omitted, the named environment variables
    are used instead. The API access token is retrieved from the Typer
    app context. It is expected to have been set prior to calling this
    method.
    """
    typer.echo(f'Downloading vessels from {api_vessels_endpoint}')

    with open('resources/vessel_imos.json', 'r') as fp:
        imos = json.load(fp)

    headers = {
        'Content-Type': 'application/json',
        'Authorization': f'Bearer {context.obj["access_token"]}'
    }

    payload = json.dumps({
        'Imos': imos
    })

    response = post(
        api_vessels_endpoint,
        data=payload,
        headers=headers
    )

    # If we don't recieve a success status, abort
    response.raise_for_status()

    results = response.json()['items']

    with open(ghg_ratings_csv_path, 'w', encoding='UTF8', newline='') as fp:
        count = 0
        header = [
            'IMO', 'RightshipInternalVesselId', 'PlatformLink', 'GHGRating',
            'GHGRatingPlus', 'GHGRatingDate', 'EVDI', 'Verified'
        ]

        writer = csv.writer(fp)
        writer.writerow(header) # Add the column names

        for result in results:
            ghg = result.get('ghg', None)

            if ghg:
                count += 1

                # Convert the list of additional data objects into an
                # dict for easy value retrieval
                additionalData = {item['key']: item['value'] for item in ghg['additionalData']}

                writer.writerow([
                    result['imo'],
                    result['rightshipInternalVesselId'],
                    result['platformLink'],
                    ghg['rating'],
                    additionalData.get('plus', False),
                    ghg['ratingDate'],
                    additionalData['evdi'],
                    additionalData['verified']
                ])

        typer.echo(f'{count} GHG ratings written to {ghg_ratings_csv_path}')
