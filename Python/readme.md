# Python + RightShip APIs

Sample code for calling the RightShip APIs, using Python.

## Getting started

Before running this sample code, you will need to do a few things:

* Ensure that Python 3.9 or later is installed
* Make a copy of the file `.env_example` and rename it to `.env`.
* Replace the values for `AUTH0_CLIENT_ID`, `AUTH0_CLIENT_SECRET` and `AUTH0_AUDIENCE`, in the file `.env`, using the relative values supplied by RightShip.
* It is recommended to create a Python virtual environment for the project to keep it isolated. For example, using Powershell:

    ```powershell
    python -m venv .venv # creates a .venv folder
    .\.venv\Scripts\Activate.ps1 # activates the virtual environment
    ```

* Install dependencies using `PIP`, for example, using Powershell:

    ```powershell
    pip install -r requirements.txt
    ```

## Sample: download GHG ratings to a local CSV file

This sample will download the GHG ratings for the vessels specified in `resources/vessel_imos.json`. Modify this file to query different or additional vessels. The results will be written to `ghg_ratings.csv`.

To review the code it is recommended to start from the entry point `src/__main__.py`.

To run this sample from the command line:

```bash
python -m src vessels download-ghg-ratings
```

---
> **NOTE**  
The RightShip vessels API allows querying up to 500 vessels at a time. This sample does not support batching multiple calls, so if you are looking to query more than 500 vessels, you will need to adapt the code to query in batches.

---

## License

This project is licensed under the MIT license. See the LICENSE file for more info.
